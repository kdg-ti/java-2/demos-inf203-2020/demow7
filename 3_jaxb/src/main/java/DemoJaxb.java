import be.kdg.model.Piloten;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;

public class DemoJaxb {
    private static final String FILENAME = "data/piloten.xml";

    public static void main(String[] args) {
        Piloten piloten = new Piloten();
        marshall(piloten);
        piloten = null;
        piloten = unmarshall();
        System.out.println("Overzicht piloten na marshall/unmarshall: \n");
        piloten.getPiloten().forEach(System.out::println);
    }

    private static void marshall(Piloten piloten) {
        try {
            JAXBContext context = JAXBContext.newInstance(Piloten.class);
            Marshaller jaxbMarshaller = context.createMarshaller();

            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            jaxbMarshaller.marshal(piloten, new File(FILENAME));
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    private static Piloten unmarshall() {
        try {
            JAXBContext context = JAXBContext.newInstance(Piloten.class);
            Unmarshaller unmarshaller = context.createUnmarshaller();

            File file = new File(FILENAME);
            return (Piloten) unmarshaller.unmarshal(file);
        } catch(JAXBException e) {
            e.printStackTrace();
        }
        return null;
    }
}
