package be.kdg.model;

import java.time.LocalDate;

public class Piloot {
    private String team;
    private String naam;
    private int nummer;
    private LocalDate wedstrijdDatum;

    public Piloot(String team, String naam, int nummer, LocalDate wedstrijdDatum) {
        this.team = team;
        this.naam = naam;
        this.nummer = nummer;
        this.wedstrijdDatum = wedstrijdDatum;
    }

    public String getTeam() {
        return team;
    }

    public String getNaam() {
        return naam;
    }

    public int getNummer() {
        return nummer;
    }

    public LocalDate getWedstrijdDatum() {
        return wedstrijdDatum;
    }

    @Override
    public String toString() {
        return String.format("%-10s (%-8s) %2d  --> %s", naam, team, nummer, wedstrijdDatum);
    }
}
