import be.kdg.model.Piloot;
import be.kdg.model.Piloten;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.util.List;

public class MaakDomXml {
    private static final String FILENAME = "data/piloten.xml";

    public static void main(String[] args) {
        List<Piloot> pilootList = Piloten.getPiloten();

        try {
            //document maken:
            Document doc = DocumentBuilderFactory.newInstance()
                    .newDocumentBuilder().newDocument();
            //TODO: root element maken:


             for (Piloot piloot : pilootList) {
                Element pilootElement = doc.createElement("piloot");
                //TODO: attribuut toevoegen:

                //TODO: naam toevoegen:

                //TODO: nummer toevoegen:

                //TODO: wedstrijd toevoegen:

            }
            // source voor wegschrijven
            DOMSource domSource = new DOMSource(doc);
            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            // pretty print
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
            //display xml on console:
            transformer.transform(domSource, new StreamResult(System.out));
            //save file:
            transformer.transform(domSource, new StreamResult(new File(FILENAME)));
            System.out.println("File Saved!");
        } catch (ParserConfigurationException | TransformerException e) {
            e.printStackTrace();
        }
    }
}