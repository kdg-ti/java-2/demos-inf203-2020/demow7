import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import model.Piloot;
import model.Piloten;

import java.io.*;
import java.util.List;

public class DemoGson {
    private static final String FILENAME = "data/piloten.json";

    public static void main(String[] args) {
        Piloten piloten = new Piloten();
        writeGson(piloten.getPiloten());
        List<Piloot> myList = readGson();
        System.out.println("Overzicht piloten na schrijven/lezen met Gson: \n");
        myList.forEach(System.out::println);
    }

    private static void writeGson(List<Piloot> pilotenList) {
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.setPrettyPrinting().create();

        //TODO: JSON-string aanmaken en ter controle afdrukken

        try (PrintWriter jsonWriter = new PrintWriter(new OutputStreamWriter(new FileOutputStream(FILENAME)))) {
            //TODO: JSON-string wegschrijven naar file

            System.out.println("Json file saved");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private static List<Piloot> readGson() {
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        List<Piloot> pilootList = null;

        try (BufferedReader reader = new BufferedReader(new FileReader(FILENAME))) {
            //TODO: json-file inlezen en omzetten naar pilootList

            return pilootList;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}

